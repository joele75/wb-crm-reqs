(function($, Drupal)
{
	// Our function name is prototyped as part of the Drupal.ajax namespace, adding to the commands:
	Drupal.ajax.prototype.commands.wb_crmreqs_scrollToTop = function(ajax, response, status)
	{
		// The value we passed in our Ajax callback function will be available inside the
		// response object. Since we defined it as selectedValue in our callback, it will be
		// available in response.selectedValue. Usually we would not use an alert() function
		// as we could use ajax_command_alert() to do it without having to define a custom
		// ajax command, but for the purpose of demonstration, we will use an alert() function
		// here:

		//console.log(response);
		var time = 500;
		verticalOffset = 0;
		var element;
		if($('form .error').size() > 0){
			element = $('form .error').first();
			verticalOffset = -80;
			// append error
			$('span.form-error').remove();
			for(var key in response.errors){
				$('#' + key).after('<span class="form-error">' + response.errors[key] + '</span>');
			}
		}
		else {
			verticalOffset = -60;
			element = $('#wizard-form-wrapper');
		}

		offset = element.offset();
		offsetTop = offset.top + verticalOffset;
		$('html, body').animate({
		scrollTop: offsetTop
		}, time);

	};
}(jQuery, Drupal));