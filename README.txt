README.txt for Workbooks CRM Requirements Module

This module requires additional configuration after installation. 

- tcpdf library must be installed in /sites/all/libraries folder.
- admin configuration page to be used to set Hubspot Form ID & the administration email for emails to be forwarded.
- Mimemail format needs to be set to Full HTML or similar. Basic install of Drupal does NOT have an HTML text format already defined so the email body will display blank if this is not fixed.
- Hubspot needs to be connected to an account in order for the form to submit properly.